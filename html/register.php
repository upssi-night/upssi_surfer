 <!DOCTYPE html>
<html lang="fr">
  <head>
	<?php include("/var/www/html/includes/head.php"); ?>
	
  </head>
  
  <body>
	<?php include("/var/www/html/includes/menus.php"); ?>
	
	<form action="register_traitement.php" method="post">
	  
	  <div class="form-group">
		<label for="prenom">Prénom</label>
		<input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prénom">
	  </div>
	  
	  <div class="form-group">
		<label for="nom">Nom</label>
		<input type="text" class="form-control" id="nom" name="nom" placeholder="Nom">
	  </div>
	
	  <div class="form-group">
		<label for="mail">Adresse mail</label>
		<input type="email" class="form-control" id="mail" name="mail" placeholder="nom@exemple.com">
	  </div>
	  
	  <div class="form-group">
		<label for="mdp">Choisir un mot de passe /!\ Pas sécurisé DU TOUT :)</label>
		<input type="password" class="form-control" name="mdp" id="mdp">
	  </div>
		<button type="submit" class="btn btn-primary">S'enregistrer</button>
	</form>
	
  </body>
</html>
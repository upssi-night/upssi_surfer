 <!DOCTYPE html>
<html lang="fr">
  <head>
	<?php include("/var/www/html/includes/head.php"); ?>
	
  </head>
  
  <body>
	<?php include("/var/www/html/includes/menus.php"); ?>
	
	<form action="ajout_session_traitement.php" method="post">
	  
	  <div class="form-group">
		<label for="nomPlg">Nom de la plage</label>
		<input type="text" class="form-control" id="nomPlg" name="nomPlg" placeholder="Nom de la plage">
	  </div>
	  
	  <div class="form-group">
		<label for="nomVil">Nom de la ville</label>
		<input type="text" class="form-control" id="nomVil" name="nomVil" placeholder="Nom de la ville">
	  </div>
	
	  <div class="form-group">
		<label for="nomSur">NomSur</label>
		<input type="text" class="form-control" id="nomSur" name="nomSur" placeholder="nomSur">
	  </div>
	
	  <div class="form-group">
		<label for="dteBai">dteBai</label>
		<input type="text" class="form-control" id="dteBai" name="dteBai" placeholder="dteBai">
	  </div>
	
	  <div class="form-group">
		<label for="heuDeb">heuDeb</label>
		<input type="text" class="form-control" id="heuDeb" name="heuDeb" placeholder="heuDeb">
	  </div>
	
	  <div class="form-group">
		<label for="heuFin">heuFin</label>
		<input type="text" class="form-control" id="heuFin" name="heuFin" placeholder="heuFin">
	  </div>
	
	  <div class="form-group">
		<label for="durBai">durBai</label>
		<input type="text" class="form-control" id="durBai" name="durBai" placeholder="durBai">
	  </div>
	
	  <div class="form-group">
		<label for="nbrBai">nbrBai</label>
		<input type="text" class="form-control" id="nbrBai" name="nbrBai" placeholder="nbrBai">
	  </div>
	
	  <div class="form-group">
		<label for="nbrSur">nbrSur</label>
		<input type="text" class="form-control" id="nbrSur" name="nbrSur" placeholder="nbrSur">
	  </div>
	
	  <div class="form-group">
		<label for="nbrBatPec">nbrBatPec</label>
		<input type="text" class="form-control" id="nbrBatPec" name="nbrBatPec" placeholder="nbrBatPec">
	  </div>
	
	  <div class="form-group">
		<label for="nbrBatLoi">nbrBatLoi</label>
		<input type="text" class="form-control" id="nbrBatLoi" name="nbrBatLoi" placeholder="nbrBatLoi">
	  </div>
	
	  <div class="form-group">
		<label for="nbrBatVoi">nbrBatVoi</label>
		<input type="text" class="form-control" id="nbrBatVoi" name="nbrBatVoi" placeholder="nbrBatVoi">
	  </div>
	
	  <div class="form-group">
		<label for="prdUti">prdUti</label>
		<input type="text" class="form-control" id="prdUti" name="prdUti" placeholder="prdUti">
	  </div>
	  
		<button type="submit" class="btn btn-primary">Ajouter</button>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
	</form>
	
  </body>
</html>
 <!DOCTYPE html>
<html lang="fr">
  <head>
	<?php include("/var/www/html/includes/head.php"); ?>
	
  </head>
  
  <body>
	<?php include("/var/www/html/includes/menus.php"); ?>
	
	<form action="login_traitement.php" method="post">
	
	  <div class="form-group">
		<label for="mail">Adresse mail</label>
		<input type="email" class="form-control" id="mail" name="mail" placeholder="nom@exemple.com">
	  </div>
	  
	  <div class="form-group">
		<label for="mdp">Mot de passe</label>
		<input type="password" class="form-control" name="mdp" id="mdp">
	  </div>
		<button type="submit" class="btn btn-primary">Se connecter</button>
	</form>
	
	<a class="" href="register.php">Créer un compte</a></li>
	
  </body>
</html>
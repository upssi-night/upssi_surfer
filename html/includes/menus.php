
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="../bootstrap/js/jquery.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
		
		<nav class="navbar navbar-default navbar-expand-lg navbar-light bg-light" id="menuHaut">
			<!-- <img src="../includes/Logo.png" class="navbar-toggler figure-img img-fluid rounded" width="10%" type="button" data-toggle="collapse" data-target="#navHaut" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> -->

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navHaut" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="col">
					<a href = "../index.php">
						<img src="../includes/Logo.png" class="figure-img img-fluid rounded hidden-xs" width="8%" alt="Logo" >
					<a>
			</div>
			<div class="collapse navbar-collapse" id="navHaut">
				<ul class="nav navbar-nav mr-auto" id="main-menu">
					<li class="nav-item"><a class="nav-link" href="#">Rechercher</a></li>

					<?php
					
					if(isset($_SESSION["idUti"])) {
						echo "<li class='nav-item'><a class='nav-link' href='afficheprofil.php'>Profil</a></li>";
						echo "<li class='nav-item'><a class='nav-link' href='deconnexion.php'>Deconnexion</a></li>";
					} else {
						echo "<li class='nav-item'><a class='nav-link' href='login.php'>Profil</a></li>";
					}
					
					?>
				</ul>
			</div>
		</nav>
		
		<nav class="navbar navbar-default fixed-bottom navbar-expand-lg navbar-light bg-light" id="menuBas">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBas" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navBas">
				<ul class="d-flex justify-content-center nav navbar-nav mr-auto" id="main-menu">
					<li class="p-2 nav-item"><a class="nav-link" href="#">Carte</a></li>
					<li class="p-2 nav-item"><a class="nav-link" href="stats.php">Stats</a></li>
					<li class="p-2 nav-item"><a class="nav-link" href="infos.php">Infos</a></li>
					<li class="p-2 nav-item"><a class="nav-link" href="listeprofils.php">Profils</a></li>
					<li class="p-2 nav-item"><a class="nav-link" href="listeplages.php">Plages</a></li>
				</ul>
			</div>
		</nav>
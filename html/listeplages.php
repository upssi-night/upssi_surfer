<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <!-- Bootstrap CSS -->
          
        <link rel="stylesheet" href="style.css" />

        <?php include("/var/www/html/includes/head.php"); ?>
    </head>

    <body>

        <?php include("/var/www/html/includes/menus.php"); ?>
    
        <?php 
             $row = 1;
            $x = 0;
    
             if (($handle = fopen("csv/plages.csv", "r")) !== FALSE) {
                 while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                     if($x>0){
                        $num = count($data);
                        $row++;
                        $lignes[$row-2] = $data;
                     }
                     $x++;
                     
                 }
                 fclose($handle);
                }
                echo '<table class="table table-striped">';
                echo '<thead><tr><th scope="col">Id<th scope="col">Plage<th scope="col">Ville</tr></thead><tbody>';
             foreach ($lignes as $personne) {
                 $i = 0;
                 echo '<tr>';
                 foreach ($personne as $data) {
                     if ($i == 0 || $i == 1 || $i == 2) {
                         echo '<td>', $data,'</td>';
                     }
                     $i = $i + 1;
                 }
                 echo '</tr>';
                 
             }
             echo '</tbody></table>';
              ?>

    </body>
</html>
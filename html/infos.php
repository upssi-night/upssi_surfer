<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="style.css" />

    <?php include("/var/www/html/includes/head.php"); ?>
</head>

<body>

<?php include("/var/www/html/includes/menus.php"); ?>

<h1> Informations sur le projet </h1>

<p>
<div class="row">
    <div class="col-lg-3 col-mg-12 text-center">
        <img style="width:6em; height:6em;" src="images/logo_surfrider.png">
    </div>
    <div class="col-lg-9 col-mg-12 text-center">
        <h5> Depuis plus de 25 ans, Surfrider protège, sauvegarde et met en valeur les océans et toute la population qui en jouit.
            Elle agit au quotidien pour lutter contre les atteintes à l'environnement littoral.
        </h5>
    </div>
    <div class="col-lg-3 col-mg-12 text-center">
        <img style="width:6em; height:6em;" src="images/logo_nuit_info.jpg">

    </div>
    <div class="col-lg-9 col-mg-12 text-center">
        <h5> La nuit de l'info est une manifestation dont l'objectif est de faire travailler ensemble des étudiants sur un projet informatique national.
            L'objectif de cette année 2020 est de développer une application permettant aux surfeurs de saisir les informations sur leurs séances afin de remonter des informations
            utiles pour l'association Surfrider.
        </h5>
    </div>
    <div class="col-lg-3 col-mg-12 text-center">
        <img style="width:6em; height:6em;" src="images/logo_team.png">

    </div>
    <div class="col-lg-9 col-mg-12 text-center">
        <h5> Pour ce faire, l'équipe UPSSI'NIGHT, composée de Matéo Gatel, Tanguy Veyrenc de Lavalette, Rémi Laborie, Thibault Delpy, Bastien Roubiniaux, Thomas Airaud, Dorian Bordes et Killian Gonet
            à réalisé un site web recensant toutes ces données de façon simple, via un système de profil : une personne peut ainsi entrer les données de ses session de surf
            afin de faire remonter les données de pollution.
        </h5>
    </div>
    <div class="col-lg-12 col-mg-12 text-center">
        <img src="images/photomontage_team.png">
    </div>
    <div class="col-lg-12 col-mg-12 text-center">
        <img src="images/meme.jpg">
        <p>Mention spéciale à Pierre-Yves Dedieu !</p>
        <br>
        <p> A l'année prochaine !</p>
        <br>
        <p>;)</p>
    </div>
</div>
</p>

</body>
</html>
